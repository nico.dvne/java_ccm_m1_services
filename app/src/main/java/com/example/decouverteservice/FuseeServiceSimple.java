package com.example.decouverteservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class FuseeServiceSimple extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_fusee_service_simple);
    }

    public void onClickLaunch(View view) {
        Intent launchIntent = new Intent(this, ServiceSimple.class);

        startService(launchIntent);
    }

    public void onClickExit(View view) {
        finish();
    }
}