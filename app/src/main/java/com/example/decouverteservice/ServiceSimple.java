package com.example.decouverteservice;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.TimeUnit;

public class ServiceSimple extends Service {

    private static final String TAG = ServiceSimple.class.getName();

    public ServiceSimple() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
      return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Thread logThread = new Thread(() -> {
            for (int i = 0; i < 20 ; i++) {
                Log.d(TAG, "Seconde numéro " + i);
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        logThread.start();

        //Sticky :
        return START_STICKY;
    }
}