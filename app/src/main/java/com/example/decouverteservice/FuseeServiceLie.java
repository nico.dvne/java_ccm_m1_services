package com.example.decouverteservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;

import java.util.Objects;

public class FuseeServiceLie extends AppCompatActivity {

    private Button btnRequete;
    private Intent myIntent;
    private ServiceLie serviceLie;
    private ServiceConnection serviceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_fusee_service_lie);
        btnRequete = findViewById(R.id.btn_service_lie_request_service);
        myIntent = new Intent(this, ServiceLie.class);

        this.serviceConnection  = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                serviceLie = ((ServiceLie.MonBinder) iBinder ).seConnecter();
                btnRequete.setEnabled(true);
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                btnRequete.setEnabled(false);
            }
        };
    }



    public void onClickLaunch(View view) {
        startService(myIntent);
    }

    public void onClickLink(View view) {
        bindService(myIntent, serviceConnection, BIND_AUTO_CREATE);
    }

    public void onClickRequest(View view) {
        if (Objects.nonNull(serviceLie)) {
            serviceLie.serviceRequest("Requete venue de la fusee service lie");
        }
    }

    public void onClickExit(View view) {
        stopService(myIntent);
        finish();
    }
}