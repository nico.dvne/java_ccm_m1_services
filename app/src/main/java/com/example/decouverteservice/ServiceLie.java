package com.example.decouverteservice;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class ServiceLie extends Service {

    private MonBinder selfBinder;

    public ServiceLie() {
        this.selfBinder = new MonBinder();
    }

    public class MonBinder extends Binder {
        public ServiceLie seConnecter() {
            return ServiceLie.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return selfBinder;
    }

    public void serviceRequest(String s) {
        Log.d("NICO", s);
    }

}
