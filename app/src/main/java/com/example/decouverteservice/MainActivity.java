package com.example.decouverteservice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickLaunchSimple(View view) {
        startActivity(new Intent(this, FuseeServiceSimple.class));
    }

    public void onClickLaunchLie(View view) {
        startActivity(new Intent(this, FuseeServiceLie.class));
    }
}